# Ejemplo de aplicación de GraphQL en Python usando FLASK y GRAPHENE

Este es una aplicación de GraphQL usando el framework **FLASK** junto a **Graphene**, donde se construye el ejemplo práctico del curso de GraphQL avanzado de Platzi pero en **Python 3** y añadiendole unas capas adicionales de validación de datos de entrada y una base de datos **SQLite**.

## Cómo ejecutar

Para ejecutar la aplicación se deben instalar las dependencias necesarias:

    $ pip install -r requirements

Crear una variable de ambiente de FLASK:

    $ export FLASK_APP=main.py

Ejecutar el servidor usando el siguiente comando:

    $ python3 main.py run

Si todo salió bien, el servidor se debe ejecutar en **http://localhost:4000** y para acceder a la parte gráfica, es decir **GraphiQL** se dirije a la ruta **http://localhost:4000/graphql**

## Cosas por agregar o mejorar

- [ ] Validación de datos en los Query
- [ ] Implementación de Middleware de Graphene para la validación de datos de entrada
- [ ] Manejo de errores en los Query 
- [ ] En caso de que no se quiera usar **GraphiQL**, deshabilitarlo en Service/\_\_init\_\_.py
- [ ] Ejecutarlo sobre un docker
- [ ] Implemetar DevOps en el proyecto


