CREATE DATABASE COURSES;


CREATE TABLE teacher(
    teacher_id VARCHAR(64) PRIMARY KEY NOT NULL,
    name VARCHAR(50) NOT NULL,
    nationality VARCHAR(25) NOT NULL,
    gender VARCHAR(20) NOT NULL
);

create table course(
    course_id VARCHAR(64) PRIMARY KEY,
    title VARCHAR(25) NOT NULL,
    description VARCHAR(50) NOT NULL,
    teacher_id VARCHAR(64) NOT NULL,
    rating REAL,
    FOREIGN KEY(teacher_id) REFERENCES teacher(teacher_id)
);


CREATE TABLE comment(
    comment_id VARCHAR(64) PRIMARY KEY NOT NULL,
    name VARCHAR(20) NOT NULL,
    body VARCHAR(200) NOT NULL,
    course_id VARCHAR(64) NOT NULL,
    FOREIGN KEY(course_id) REFERENCES course(course_id) 
);

INSERT INTO teacher VALUES('bd9c4756-a70c-4b9a-a233-65ed8405405e', 'Jhon Doe', 'All', 'MALE');
INSERT INTO teacher VALUES('0fbe88c8-acc5-4f33-b81d-9800ede0bf69', 'Juanita', 'Colombia', 'FEMALE');

INSERT INTO course VALUES('8f8b53f1-4aa0-4f39-b809-e7e36c5ba6d6','Graphql en Java','Mi primer programa de graphql con Java','bd9c4756-a70c-4b9a-a233-65ed8405405e', 5.0);

INSERT INTO course VALUES('ec990b5f-b6ff-49ff-9f88-19695e56b95d','GraphQL en Python con Graphene','Curso de ejemplo de graphql en python','0fbe88c8-acc5-4f33-b81d-9800ede0bf69', 5.0);           

INSERT INTO comment VALUES('ac7acefa-d7e2-4d5d-b552-190a3fdcf315', 'Excelente curso!', 'Sigan así!!', 'ec990b5f-b6ff-49ff-9f88-19695e56b95d');



