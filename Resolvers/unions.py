from graphene import Union
from Schemas.teacher import TeacherSchema, TeacherModel
from Schemas.comment import CommentSchema
from Schemas.course import CourseSchema, CourseModel


class SearchResult(Union):
    class Meta:
        types = (TeacherSchema, CommentSchema, CourseSchema)
