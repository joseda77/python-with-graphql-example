from graphene import Schema
from .resolvers import Query
from .mutations import Mutations

try:
    schema = Schema(query=Query, mutation=Mutations)
except Exception as error:
    print('Failed to bring schema', error)