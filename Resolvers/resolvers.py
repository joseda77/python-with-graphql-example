from graphene import ObjectType, Field, Enum, String, List
from Schemas.teacher import TeacherSchema, TeacherModel
from Schemas.comment import CommentSchema, CommentModel
from Schemas.course import CourseSchema, CourseModel
from .unions import SearchResult
from Domain.validations import Validations


class Gender(Enum):
    MALE = 'MALE'
    FEMALE = 'FEMALE'
    OTHER = 'OTHER'


class Query(ObjectType):
    courses = List(lambda: CourseSchema)
    course = Field(lambda: CourseSchema, course_id=String())
    teachers = List(lambda: TeacherSchema)
    teacher = Field(lambda: TeacherSchema, teacher_id=String())
    comments = List(lambda: CommentSchema)
    search = List(lambda: SearchResult, search_query=String())

    # our Resolver method takes the Schemas context (root, info) as well as
    # Argument (name) for the Field and returns data for the query Response

    def resolve_courses(self, info):
        query = CourseSchema.get_query(info)
        courses = query.all()
        validations = Validations()
        if len(courses) > 0:  # This line is optional, you can unescape in frontend or domain
            for comment in courses:
                comment.description = validations.unescape_text(comment.description)
        return courses

    def resolve_course(self, info, course_id):
        query = CourseSchema.get_query(info=info)
        query = query.filter(CourseModel.course_id == course_id)
        return query.first()

    def resolve_comments(self, info):
        query = CommentSchema.get_query(info)
        comments = query.all()
        validations = Validations()
        if len(comments) > 0:       # This line is optional, you can unescape in frontend or domain
            for comment in comments:
                comment.body = validations.unescape_text(comment.body)
        return comments

    def resolve_teachers(self, info):
        query = TeacherSchema.get_query(info)
        return query.all()

    def resolve_teacher(self, info, teacher_id):
        query = TeacherSchema.get_query(info)
        query = query.filter(TeacherModel.teacher_id == teacher_id)
        return query.first()

    def resolve_search(self, info, search_query):
        teacher_query = TeacherSchema.get_query(info)
        teacher_query = teacher_query.filter(TeacherModel.name.contains(search_query) |
                                             TeacherModel.nationality.contains(search_query) |
                                             TeacherModel.gender.contains(search_query))
        course_query = CourseSchema.get_query(info)
        course_query = course_query.filter(CourseModel.description.contains(search_query) |
                                           CourseModel.rating.contains(search_query) |
                                           CourseModel.title.contains(search_query))
        comment_query = CommentSchema.get_query(info)
        comment_query = comment_query.filter(CommentModel.name.contains(search_query) |
                                             CommentModel.body.contains(search_query))
        query = comment_query.all() + course_query.all() + teacher_query.all()
        return query
