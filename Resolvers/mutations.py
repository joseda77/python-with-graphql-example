from Mutations.teacher import CreateTeacher, UpdateTeacher, DeleteTeacher
from Mutations.course import CreateCourse, UpdateCourse, DeleteCourse
from Mutations.comment import CreateComment, UpdateComment, DeleteComment
from graphene import ObjectType


class Mutations(ObjectType):
    add_teacher = CreateTeacher.Field()
    edit_teacher = UpdateTeacher.Field()
    delete_teacher = DeleteTeacher.Field()
    add_course = CreateCourse.Field()
    edit_course = UpdateCourse.Field()
    delete_course = DeleteCourse.Field()
    add_comment = CreateComment.Field()
    edit_comment = UpdateComment.Field()
    delete_comment = DeleteComment.Field()
