import os


class Config:
    SECRET_KEY = os.urandom(128)
    instance_relative_config = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Development(Config):
    """
    Has all development configuration, with mysql database of test and
    debug activated
    """

    DEBUG = True
    TESTING = False
    # Optional DB connection
    SQLALCHEMY_DATABASE_URI = 'sqlite:///../COURSES.db'


class Testing(Config):
    """
    Has configuration of unit test in a mock database storaged in memory (SQLite).
    """

    TESTING = True
    DEBUG = True
    path_project = os.getcwd()
    path_database = 'sqlite:///../COURSES.db'
    SQLALCHEMY_DATABASE_URI = path_database


class Production(Config):
    """
    Has configuration of production environment.
    """

    DEBUG = False
    TESTING = False
    # USER_DB = os.environ.get('MYSQL_USER')
    # USER_DB_PASS = os.environ.get('MYSQL_PASSWORD')
    # HOST_DB = os.environ.get('MYSQL_HOST')
    # SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/YOUR-DB'.format(USER_DB, USER_DB_PASS, HOST_DB)


config = {
    'DEVELOPMENT': Development,
    'TESTING': Testing,
    'PRODUCTION': Production
}
