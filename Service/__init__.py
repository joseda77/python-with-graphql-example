MODULE_NAME = 'Service'
IMPORT_NAME = __name__

from flask import Flask
from flask_cors import CORS
from config import config
from flask_graphql import GraphQLView
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app(config_name='DEVELOPMENT'):
    """
    Create all flask app, load configuration database and make a context
    :param config_name: configuration name of class configuration
    :return: app: is the context of application
    """

    app = Flask(__name__)
    CORS(app)
    app.config.from_object(config[config_name])
    from Resolvers import schema
    app.add_url_rule('/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=True))

    with app.app_context():
        # Imports
        # Here include new imports from service file
        db.init_app(app)
        # db.create_all(bind='__all__')
        # Blueprint modules
        # app.register_blueprint(authService)

        # create models tables
        @app.shell_context_processor
        def ctx():
            return {'app': app, 'db': db}

        return app
