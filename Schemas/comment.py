from graphene_sqlalchemy import SQLAlchemyObjectType
from Models.comment import CommentModel as CommentModel


class CommentSchema(SQLAlchemyObjectType):
    class Meta:
        try:
            model = CommentModel
            exclude_fields = ('course_id', 'CourseModel', )
            # interfaces = (relay.Node, ) # provide a graphql identifier
        except Exception as error:
            print('Error in comment schema: ', error)