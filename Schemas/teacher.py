from graphene_sqlalchemy import SQLAlchemyObjectType
from Models.teacher import TeacherModel


class TeacherSchema(SQLAlchemyObjectType):
    class Meta:
        try:
            model = TeacherModel
            description = 'TeacherModel Schema Model'
            # interfaces = (relay.Node, ) # provide a graphql identifier
        except Exception as error:
            print('Error in teacher schema: ', error)