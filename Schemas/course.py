from graphene_sqlalchemy import SQLAlchemyObjectType
from graphene import relay
from Models.course import CourseModel as CourseModel


class CourseSchema(SQLAlchemyObjectType):
    class Meta:
        try:
            model = CourseModel
            exclude_fields = ('teacher_id', )
        except Exception as error:
            print('Error in course schema: ', error)