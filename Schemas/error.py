from graphene import ObjectType, String, Int


class Error(ObjectType):
    message = String()
    code = Int()
    name = String()
