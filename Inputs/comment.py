from graphene import InputObjectType, String, Float


class CreateCommentInput(InputObjectType):
    name = String(required=True)
    body = String(required=True)
    course_id = String(required=True)


class EditCommentInput(InputObjectType):
    name = String(required=True)
    body = String(required=True)