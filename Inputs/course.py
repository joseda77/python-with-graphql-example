from graphene import InputObjectType, String, Float


class CreateCourseInput(InputObjectType):
    title = String(required=True)
    description = String(required=True)
    teacher_id = String(required=False)


class EditCourseInput(InputObjectType):
    title = String(required=False)
    description = String(required=False)
    teacher_id = String(required=False)
    rating = Float(required=False)

