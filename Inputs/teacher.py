from graphene import InputObjectType, String


class CreateTeacherInput(InputObjectType):
    name = String(required=True)
    nationality = String(required=True)
    gender = String(required=False)


class EditTeacherInput(InputObjectType):
    name = String(required=False)
    nationality = String(required=False)
    gender = String(required=False)

