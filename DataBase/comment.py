from Service import db
from Models.comment import CommentModel


class CommentDataBase:

    @staticmethod
    def get_comment_by_id(comment_id):
        comment = None
        try:
            comment = CommentModel.query.filter_by(comment_id=comment_id).first()
        except:
            comment = None
        finally:
            db.session.close()
            return comment

    @staticmethod
    def get_all_comments() -> list:
        comment = None
        try:
            comment = db.session.query(CommentModel).filter().all()
        except:
            comment = None
        finally:
            db.session.close()
            return comment

    @staticmethod
    def create_comment(**kwargs) -> bool:
        message = False
        try:
            comment_schema = CommentModel(
                comment_id=kwargs['comment_id'],
                name=kwargs['name'],
                body=kwargs['body'],
                course_id=kwargs['course_id']
            )
            db.session.add(comment_schema)
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message

    @staticmethod
    def update_comment_information(**kwargs) -> bool:
        message = False
        try:
            update_object = {
                'name': kwargs['name'],
                'body': kwargs['body']
            }
            db.session.query(CommentModel).filter_by(comment_id=kwargs['comment_id']).update(update_object)
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message

    @staticmethod
    def delete_comment(comment_id: str) -> bool:
        message = False
        try:
            db.session.query(CommentModel).filter_by(comment_id=comment_id).delete()
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message
