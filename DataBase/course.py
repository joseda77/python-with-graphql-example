from Service import db
from Models.course import CourseModel


class CourseDataBase:

    @staticmethod
    def get_course_by_id(course_id):
        course = None
        try:
            course = CourseModel.query.filter_by(course_id=course_id).first()
        except:
            course = None
        finally:
            db.session.close()
            return course

    @staticmethod
    def get_all_courses() -> list:
        courses = None
        try:
            courses = db.session.query(CourseModel).filter().all()
        except:
            courses = None
        finally:
            db.session.close()
            return courses

    @staticmethod
    def create_course(**kwargs) -> bool:
        message = False
        try:
            course_schema = CourseModel(
                course_id=kwargs['course_id'],
                title=kwargs['title'],
                description=kwargs['description'],
                teacher_id=kwargs['teacher_id'],
                rating=kwargs['rating'])
            db.session.add(course_schema)
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message

    @staticmethod
    def update_course_information(**kwargs) -> bool:
        message = False
        try:
            update_object = {
                'title': kwargs['title'],
                'description': kwargs['description'],
                'teacher_id': kwargs['teacher_id'],
                'rating': kwargs['rating']
            }
            db.session.query(CourseModel).filter_by(course_id=kwargs['course_id']).update(update_object)
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message

    @staticmethod
    def delete_course(course_id: str) -> bool:
        message = False
        try:
            db.session.query(CourseModel).filter_by(course_id=course_id).delete()
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message
