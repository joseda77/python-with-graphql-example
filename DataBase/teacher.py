from Service import db
from Models.teacher import TeacherModel


class TeacherDatabase:

    @staticmethod
    def get_teacher_by_id(teacher_id):
        teacher = None
        try:
            teacher = TeacherModel.query.filter_by(teacher_id=teacher_id).first()
        except:
            teacher = None
        finally:
            db.session.close()
            return teacher

    @staticmethod
    def get_all_teachers() -> list:
        teachers = None
        try:
            teachers = db.session.query(TeacherModel).filter().all()
        except:
            teachers = None
        finally:
            db.session.close()
            return teachers

    @staticmethod
    def create_teacher(**kwargs) -> bool:
        message = False
        try:
            teacher_schema = TeacherModel(
                teacher_id=kwargs['teacher_id'],
                name=kwargs['name'],
                gender=kwargs['gender'],
                nationality=kwargs['nationality'])
            db.session.add(teacher_schema)
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message

    @staticmethod
    def update_teacher_information(**kwargs) -> bool:
        message = False
        try:
            update_object = {
                'gender': kwargs['gender'],
                'name': kwargs['name'],
                'nationality': kwargs['nationality']
            }
            db.session.query(TeacherModel).filter_by(teacher_id=kwargs['teacher_id']).update(update_object)
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message

    @staticmethod
    def delete_teacher(teacher_id: str) -> bool:
        message = False
        try:
            db.session.query(TeacherModel).filter_by(teacher_id=teacher_id).delete()
            db.session.commit()
            message = True
        except:
            message = False
        finally:
            db.session.close()
            return message
