from Inputs.teacher import CreateTeacherInput, EditTeacherInput
from Schemas.teacher import TeacherSchema
from graphene import Mutation, Field, String
from Domain.teacher import TeacherDomain
from Schemas.error import Error
import uuid


class CreateTeacher(Mutation):
    class Arguments:
        teacher_input_data = CreateTeacherInput(required=True)

    teacher = Field(TeacherSchema)
    errors = Field(Error)

    def mutate(self, info, teacher_input_data=None):
        """
        Method that calls database to create an teacher, models and domains validations and verifications.

        param info: Graphene context.
        param teacher_input_data: name, gender, nationality.
        return teacher: return teacher info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            teacher_domain = TeacherDomain()
            teacher_id = str(uuid.uuid4())
            created = teacher_domain.create_teacher(
                teacher_id=teacher_id,
                name=teacher_input_data.name,
                gender=teacher_input_data.gender,
                nationality=teacher_input_data.nationality
            )
            if created:
                teacher = teacher_domain.get_teacher_by_id(teacher_id)
                return CreateTeacher(teacher=teacher)
            else:
                raise Exception('Teacher could not be created')
        except Exception as error:
            print('Error creating teacher:', error)
            data_error = Error(message="Teacher could not be created",
                               code=1001, name="Created Teacher Error")
            return CreateTeacher(errors=data_error)


class UpdateTeacher(Mutation):
    class Arguments:
        teacher_id = String(required=True)
        teacher_input_data = EditTeacherInput(required=True)

    teacher = Field(TeacherSchema)
    errors = Field(Error)

    def mutate(self, info, teacher_id=None, teacher_input_data=None):
        """
        Method that calls database to update an teacher, models and domains validations and verifications.

        param info: Graphene context.
        param course_id: teacher identifier.
        param teacher_input_data: name, gender, nationality.
        return teacher: return teacher info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            teacher_domain = TeacherDomain()
            updated = teacher_domain.update_teacher(
                teacher_id=teacher_id,
                name=teacher_input_data.name,
                gender=teacher_input_data.gender,
                nationality=teacher_input_data.nationality
            )
            if updated:
                teacher = teacher_domain.get_teacher_by_id(teacher_id)
                return UpdateTeacher(teacher=teacher)
            else:
                raise Exception('Teacher could not be updated')
        except Exception as error:
            print('Error updating teacher:', error)
            data_error = Error(message="Teacher could not be updated",
                               code=1002, name="Update Teacher Error")
            return UpdateTeacher(errors=data_error)


class DeleteTeacher(Mutation):
    class Arguments:
        teacher_id = String(required=True)

    teacher = Field(TeacherSchema)
    errors = Field(Error)

    def mutate(self, info, teacher_id=None):
        """
        Method that calls database to delete an teacher, models and domains validations and verifications.

        param info: Graphene context.
        param course_id: teacher identifier.
        return teacher: return teacher info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            teacher_domain = TeacherDomain()
            teacher = teacher_domain.get_teacher_by_id(teacher_id)
            if teacher:
                deleted = teacher_domain.delete_teacher(teacher_id=teacher.teacher_id)
            else:
                raise Exception('Teacher does not exist')
            if deleted:
                return DeleteTeacher(teacher=teacher)
            else:
                raise Exception('Teacher could not be removed')
        except Exception as error:
            print('Error deleting teacher:', error)
            data_error = Error(message="Teacher could not be deleted",
                               code=1003, name="Delete Teacher Error")
            return DeleteTeacher(errors=data_error)