from Inputs.course import CreateCourseInput, EditCourseInput
from Schemas.course import CourseSchema
from graphene import Mutation, Field, String
from Domain.course import CourseDomain
from Schemas.error import Error
import uuid


class CreateCourse(Mutation):
    class Arguments:
        course_input_data = CreateCourseInput(required=True)

    course = Field(CourseSchema)
    errors = Field(Error)

    def mutate(self, info, course_input_data=None):
        """
        Method that calls database to create an course, models and domains validations and verifications.

        param info: Graphene context.
        param course_input_data: name, gender, nationality.
        return course: return course info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            course_domain = CourseDomain()
            course_id = str(uuid.uuid4())
            rating = 0
            created = course_domain.create_courses(
                course_id=course_id,
                title=course_input_data.title,
                description=course_input_data.description,
                teacher_id=course_input_data.teacher_id,
                rating=rating
            )
            if created:
                course = course_domain.get_course_by_id(course_id)
                return CreateCourse(course=course)
            else:
                raise Exception('Course could not be created')
        except Exception as error:
            print('Error creating course:', error)
            data_error = Error(message="Course could not be cretead",
                               code=1001, name="Create Course Error")
            return DeleteCourse(errors=data_error)


class UpdateCourse(Mutation):
    class Arguments:
        course_id = String(required=True)
        course_input_data = EditCourseInput(required=True)

    course = Field(CourseSchema)
    errors = Field(Error)

    def mutate(self, info, course_id=None, course_input_data=None):
        """
        Method that calls database to update an course, models and domains validations and verifications.

        param info: Graphene context.
        param course_id: teacher identifier.
        param course_input_data: name, gender, nationality.
        return course: return course info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            course_domain = CourseDomain()
            updated = course_domain.update_course(
                course_id=course_id,
                title=course_input_data.title,
                description=course_input_data.description,
                teacher_id=course_input_data.teacher_id,
                rating=course_input_data.rating
            )
            if updated:
                course = course_domain.get_course_by_id(course_id)
                return UpdateCourse(course=course)
            else:
                raise Exception('Course could not be updated')
        except Exception as error:
            print('Error updating course:', error)
            data_error = Error(message="Course could not be updated",
                               code=1002, name="Update Course Error")
            return DeleteCourse(errors=data_error)


class DeleteCourse(Mutation):
    class Arguments:
        course_id = String(required=True)

    course = Field(CourseSchema)
    errors = Field(Error)

    def mutate(self, info, course_id=None):
        """
        Method that calls database to delete an course, models and domains validations and verifications.

        param info: Graphene context.
        param course_id: teacher identifier.
        return course: return course info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            course_domain = CourseDomain()
            course = course_domain.get_course_by_id(course_id)
            if course:
                deleted = course_domain.delete_course(course_id=course.course_id)
            else:
                raise Exception('Course does not exist')
            if deleted:
                return DeleteCourse(course=course)
            else:
                raise Exception('Course could not be removed')
        except Exception as error:
            print('Error deleting course:', error)
            data_error = Error(message="Course could not be deleted",
                               code=1003, name="Delete Course Error")
            return DeleteCourse(errors=data_error)
