from Inputs.comment import CreateCommentInput, EditCommentInput
from Schemas.comment import CommentSchema
from graphene import Mutation, Field, String
from Domain.comment import CommentDomain
from Schemas.error import Error
import uuid


class CreateComment(Mutation):
    class Arguments:
        comment_input_data = CreateCommentInput(required=True)

    comment = Field(CommentSchema)
    errors = Field(Error)

    def mutate(self, info, comment_input_data=None):
        """
        Method that calls database to create an comment, models and domains validations and verifications.

        param info: Graphene context.
        param course_input_data: name, gender, nationality.
        return course: return course info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            comment_domain = CommentDomain()
            comment_id = str(uuid.uuid4())
            created = comment_domain.create_comment(
                comment_id=comment_id,
                name=comment_input_data.name,
                body=comment_input_data.body,
                course_id=comment_input_data.course_id
            )
            if created:
                comment = comment_domain.get_comment_by_id(comment_id)
                return CreateComment(comment=comment)
            else:
                raise Exception('Comment could not be created')
        except Exception as error:
            print('Error creating comment:', error)
            data_error = Error(message="Comment could not be created", code=1001,
                               name="Create Comment Error")
            return CreateComment(errors=data_error)


class UpdateComment(Mutation):
    class Arguments:
        comment_id = String(required=True)
        comment_input_data = EditCommentInput(required=True)

    comment = Field(CommentSchema)
    errors = Field(Error)

    def mutate(self, info, comment_id=None, comment_input_data=None):
        """
        Method that calls database to update an comment, models and domains validations and verifications.

        param info: Graphene context.
        param course_id: teacher identifier.
        param course_input_data: name, gender, nationality.
        return course: return course info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            comment_domain = CommentDomain()
            updated = comment_domain.update_comment(
                comment_id=comment_id,
                name=comment_input_data.name,
                body=comment_input_data.body,
            )
            if updated:
                comment = comment_domain.get_comment_by_id(comment_id)
                return UpdateComment(comment=comment)
            else:
                raise Exception('Comment could not be updated')
        except Exception as error:
            print('Error updating comment:', error)
            data_error = Error(message="Comment could not be updated",
                               code=1002, name="Update Comment Error")
            return UpdateComment(errors=data_error)


class DeleteComment(Mutation):
    class Arguments:
        comment_id = String(required=True)

    comment = Field(CommentSchema)
    errors = Field(Error)

    def mutate(self, info, comment_id=None):
        """
        Method that calls database to delete an comment, models and domains validations and verifications.

        param info: Graphene context.
        param course_id: teacher identifier.
        return course: return course info when process is completed.
        return errors: return error if any error occurs
        """
        try:
            comment_domain = CommentDomain()
            comment = comment_domain.get_comment_by_id(comment_id)
            if comment:
                deleted = comment_domain.delete_comment(comment_id=comment.comment_id)
            else:
                raise Exception('Comment does not exist')
            if deleted:
                return DeleteComment(comment=comment)
            else:
                raise Exception('Comment could not be removed')
        except Exception as error:
            print('Error deleting comment:', error)
            data_error = Error(message="Comment could not be deleted",
                               code=1003, name="Delete Comment Error")
            return DeleteComment(errors=data_error)
