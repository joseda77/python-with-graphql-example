from DataBase.comment import CommentDataBase
from Domain.course import CourseDomain
from .validations import Validations


class CommentDomain:

    def __init__(self):
        self.validations = Validations()
        self.comment_database = CommentDataBase()
        self.course_domain = CourseDomain()

    def get_comment_by_id(self, comment_id):
        is_valid_id = self.validations.is_uuid_format(comment_id)
        try:
            if not comment_id or not is_valid_id:
                raise Exception('Invalid comment_id')
            comment = self.comment_database.get_comment_by_id(comment_id)
            if comment:
                comment.body = self.validations.unescape_text(comment.body)
        except:
            return None
        else:
            return comment

    def get_all_comments(self):
        try:
            comments = self.comment_database.get_all_comments()
            if len(comments) > 0:
                for comment in comments:
                    comment.body = self.validations.unescape_text(comment.body)
        except:
            return None
        else:
            return comments

    def create_comment(self, **kwargs):
        is_valid_name = self.validations.is_valid_title(kwargs['name'])
        is_valid_course_id = self.validations.is_uuid_format(kwargs['course_id'])
        body_escaped = self.validations.escape_text(kwargs['body'])
        try:
            if not is_valid_name or body_escaped is None or not is_valid_course_id:
                raise Exception('Invalid arguments')
            course_exist = self.course_domain.get_course_by_id(kwargs['course_id'])
            if course_exist is None:
                raise Exception('Invalid teacher_id in create course')

            created = self.comment_database.create_comment(
                comment_id=kwargs['comment_id'],
                name=kwargs['name'],
                body=body_escaped,
                course_id=kwargs['course_id']
            )
        except:
            return None
        else:
            return created

    def update_comment(self, **kwargs):
        is_valid_name = self.validations.is_valid_title(kwargs['name'])
        is_valid_comment_id = self.validations.is_uuid_format(kwargs['comment_id'])
        body_escaped = self.validations.escape_text(kwargs['body'])
        try:
            if not is_valid_name or not body_escaped or not is_valid_comment_id:
                raise Exception('Invalid arguments')
            updated = self.comment_database.update_comment_information(
                comment_id=kwargs['comment_id'],
                name=kwargs['name'],
                body=body_escaped
            )
        except:
            return False
        else:
            return updated

    def delete_comment(self, comment_id: str):
        is_valid_id = self.validations.is_uuid_format(comment_id)
        try:
            if not is_valid_id:
                raise Exception('Invalid arguments')
            deleted = self.comment_database.delete_comment(comment_id=comment_id)
        except:
            return False
        else:
            return deleted
