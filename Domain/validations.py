from urllib.parse import quote, unquote
from uuid import UUID
from enum import Enum
import re


class Gender(Enum):
    FEMALE = 'FEMALE'
    MALE = 'MALE'
    OTHER = 'OTHER'


class Validations:

    @staticmethod
    def is_uuid_format(id_code):
        try:
            uuid_obj = UUID(id_code, version=4)
        except ValueError:
            return False
        return str(uuid_obj) == id_code

    @staticmethod
    def is_valid_name(name):
        valid = False
        try:
            only_letters = name.replace(" ", "").isalpha()
            if only_letters and 50 > len(name) > 5:
                valid = True
        except:
            return False
        else:
            return valid

    @staticmethod
    def is_valid_gender(gender):
        valid = False
        try:
            if hasattr(Gender, gender):
                valid = True
        except:
            return False
        else:
            return valid

    @staticmethod
    def is_only_letters(chain):
        result = False
        try:
            regex = re.compile("^[a-zA-Z ]*$")
            solution = regex.findall(chain)
            if not solution:
                result = False
            else:
                result = True
        except:
            return False
        else:
            return result

    @staticmethod
    def is_valid_title(chain):
        result = False
        try:
            regex = re.compile("^[0-9a-zA-Z+*#.,-/_{}?¿¡!|&%$=]*$")
            solution = regex.findall(chain)
            if not solution and len(chain) > 30:
                result = False
            else:
                result = True
        except:
            return False
        else:
            return result

    @staticmethod
    def is_positive_float_number_less_than_five(number):
        result = False
        try:
            float_number = float(number)
            if 0 <= float_number <= 5:
                result = True
        except:
            return False
        else:
            return result

    @staticmethod
    def escape_text(char: str):
        result = None
        try:
            result = quote(char, safe=' /')
        except:
            return None
        else:
            return result

    @staticmethod
    def unescape_text(char: str):
        result = None
        try:
            result = unquote(char, encoding='utf-8')
        except:
            return None
        else:
            return result
