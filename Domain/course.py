from DataBase.course import CourseDataBase
from Domain.teacher import TeacherDomain
from .validations import Validations


class CourseDomain:

    def __init__(self):
        self.validations = Validations()
        self.course_database = CourseDataBase()
        self.teacher_domain = TeacherDomain()

    def get_course_by_id(self, course_id):
        is_valid_id = self.validations.is_uuid_format(course_id)
        try:
            if not course_id or not is_valid_id:
                raise Exception('Invalid course_id')
            course = self.course_database.get_course_by_id(course_id)
            if course:
                course.description = self.validations.unescape_text(course.description)
        except:
            return None
        else:
            return course

    def get_all_courses(self):
        try:
            courses = self.course_database.get_all_courses()
            if len(courses) > 0:
                for course in courses:
                    course.description = self.validations.unescape_text(course.description)
        except:
            return None
        else:
            return courses

    def create_courses(self, **kwargs):
        is_valid_title = self.validations.is_valid_title(kwargs['title'])
        is_valid_teacher_id_format = self.validations.is_uuid_format(kwargs['teacher_id'])
        description_escaped = self.validations.escape_text(kwargs['description'])
        try:
            if not is_valid_title or not description_escaped or not is_valid_teacher_id_format:
                raise Exception('Invalid arguments')
            teacher_id_exist = self.teacher_domain.get_teacher_by_id(kwargs['teacher_id'])
            if teacher_id_exist is None:
                raise Exception('Invalid teacher_id in create course')
            created = self.course_database.create_course(
                course_id=kwargs['course_id'],
                title=kwargs['title'],
                description=description_escaped,
                teacher_id=kwargs['teacher_id'],
                rating=kwargs['rating']
            )
        except:
            return None
        else:
            return created

    def update_course(self, **kwargs):
        is_valid_title = self.validations.is_valid_title(kwargs['title'])
        is_valid_course_id = self.validations.is_uuid_format(kwargs['course_id'])
        is_valid_teacher_id = self.validations.is_uuid_format(kwargs['teacher_id'])
        is_valid_rating = self.validations.is_positive_float_number_less_than_five(kwargs['rating'])
        description_escaped = self.validations.escape_text(kwargs['description'])
        try:
            if not is_valid_title or not description_escaped or not is_valid_course_id or \
                    not is_valid_teacher_id or not is_valid_rating:
                raise Exception('Invalid arguments')
            updated = self.course_database.update_course_information(
                course_id=kwargs['course_id'],
                title=kwargs['title'],
                description=description_escaped,
                teacher_id=kwargs['teacher_id'],
                rating=kwargs['rating']
            )
        except:
            return False
        else:
            return updated

    def delete_course(self, course_id: str):
        is_valid_id = self.validations.is_uuid_format(course_id)
        try:
            if not is_valid_id:
                raise Exception('Invalid arguments')
            deleted = self.course_database.delete_course(course_id=course_id)
        except:
            return False
        else:
            return deleted
