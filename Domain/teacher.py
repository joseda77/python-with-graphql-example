from DataBase.teacher import TeacherDatabase
from .validations import Validations


class TeacherDomain:

    def __init__(self):
        self.validations = Validations()
        self.teacher_database = TeacherDatabase()

    def get_teacher_by_id(self, teacher_id):
        is_valid_id = self.validations.is_uuid_format(teacher_id)
        try:
            if not teacher_id or not is_valid_id:
                raise Exception('Invalid teacher_id')
            teacher = self.teacher_database.get_teacher_by_id(teacher_id)
        except:
            return None
        else:
            return teacher

    def get_all_teachers(self):
        try:
            teachers = self.teacher_database.get_all_teachers()
        except:
            return None
        else:
            return teachers

    def create_teacher(self, **kwargs):

        is_valid_name = self.validations.is_valid_name(kwargs['name'])
        is_valid_nationality = self.validations.is_only_letters(kwargs['nationality'])
        is_valid_gender = self.validations.is_valid_gender(kwargs['gender'])
        try:
            if not is_valid_name or not is_valid_nationality or not is_valid_gender:
                raise Exception('Invalid arguments')

            created = self.teacher_database.create_teacher(
                teacher_id=kwargs['teacher_id'],
                name=kwargs['name'],
                nationality=kwargs['nationality'],
                gender=kwargs['gender']
            )
        except:
            return None
        else:
            return created

    def update_teacher(self, **kwargs):
        is_valid_id = self.validations.is_uuid_format(kwargs['teacher_id'])
        is_valid_name = self.validations.is_valid_name(kwargs['name'])
        is_valid_nationality = self.validations.is_only_letters(kwargs['nationality'])
        is_valid_gender = self.validations.is_valid_gender(kwargs['gender'])
        try:
            if not is_valid_name or not is_valid_nationality or not is_valid_gender or not is_valid_id:
                raise Exception('Invalid arguments')
            updated = self.teacher_database.update_teacher_information(
                teacher_id=kwargs['teacher_id'],
                name=kwargs['name'],
                nationality=kwargs['nationality'],
                gender=kwargs['gender']
            )
        except:
            return False
        else:
            return updated

    def delete_teacher(self, teacher_id: str):
        is_valid_id = self.validations.is_uuid_format(teacher_id)
        try:
            if not is_valid_id:
                raise Exception('Invalid arguments')
            deleted = self.teacher_database.delete_teacher(teacher_id=teacher_id)
        except:
            return False
        else:
            return deleted
