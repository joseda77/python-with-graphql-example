from Service import db


class CommentModel(db.Model):
    __tablename__ = 'comment'

    comment_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    body = db.Column(db.String)
    course_id = db.Column(db.String, db.ForeignKey('course.course_id'))
