from Service import db
from .comment import CommentModel


class CourseModel(db.Model):
    __tablename__ = 'course'
    course_id = db.Column(db.String, primary_key=True)
    title = db.Column(db.String)
    description = db.Column(db.String)
    teacher_id = db.Column(db.String, db.ForeignKey('teacher.teacher_id'))
    rating = db.Column(db.REAL)
    comments = db.relationship(CommentModel, backref='CourseModel',
                               primaryjoin=(course_id == db.foreign(CommentModel.course_id)))
