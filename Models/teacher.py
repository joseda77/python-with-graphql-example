from Service import db
from .course import CourseModel


class TeacherModel(db.Model):
    __tablename__ = 'teacher'
    teacher_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    nationality = db.Column(db.String)
    gender = db.Column(db.Enum('MALE', 'FEMALE', name='Gender'))
    courses = db.relationship(CourseModel, backref='Teacher',
                              primaryjoin=(teacher_id == db.foreign(CourseModel.teacher_id)))

